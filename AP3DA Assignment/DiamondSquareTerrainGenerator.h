#ifndef DIAMOND_T_GENERATOR_H
#define DIAMOND_T_GENERATOR_H

#include "ITerrainGenerator.h"
#include <fstream>
#include <string>

class DiamondSquareTerrainGenerator : public ITerrainGenerator
{
private:
	float _displacement = 1.0f;

	int _width;
	int _height;
public:
	DiamondSquareTerrainGenerator(){}
	~DiamondSquareTerrainGenerator();

	Geometry GenerateTerrain(ID3D11Device* device, int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow);
	ID3D11ShaderResourceView* GetShaderResourceView(ID3D11Device* device);
};

#endif