#include "Skeleton.h"

Skeleton::Skeleton(string type, Geometry geometry, Material material)
{
	_parent = nullptr;
	_position = XMFLOAT3();
	_rotation = XMFLOAT3();
	_scale = XMFLOAT3(1.0f, 1.0f, 1.0f);

	_geometry = geometry;
	_type = type;
	_material = material;

	_textureRV = nullptr;
}

void Skeleton::InitArm()
{
	//Bone* navigator;
	_root = new Bone(_position, nullptr);

	Bone* neck = new Bone(XMFLOAT3(0.0f, -2.5f, 0.0f), _root);
	_root->AddChild(neck);

	Bone* rightUpperArm = new Bone(XMFLOAT3(5.0f, -2.5f, 0.0f), neck);
	Bone* rightLowerArm = new Bone(XMFLOAT3(0.0f, -5.0f, 0.0f), rightUpperArm);

	neck->AddChild(rightUpperArm);
	rightUpperArm->AddChild(rightLowerArm);

	Bone* leftUpperArm = new Bone(XMFLOAT3(-5.0f, -2.5f, 0.0f), neck);
	Bone* leftLowerArm = new Bone(XMFLOAT3(0.0f, -5.0f, 0.0f), leftUpperArm);

	//leftUpperArm->SetRotation(0.0f, 0.0f, XM_PI / 200);

	neck->AddChild(leftUpperArm);
	leftUpperArm->AddChild(leftLowerArm);

	Bone* pelvis = new Bone(XMFLOAT3(0.0f, -10.0f, 0.0f), neck);
	neck->AddChild(pelvis);

	Bone* pelvisRight = new Bone(XMFLOAT3(2.5f, 0.0f, 0.0f), pelvis);
	pelvis->AddChild(pelvisRight);

	Bone* pelvisLeft = new Bone(XMFLOAT3(-2.5f, -0.0f, 0.0f), pelvis);
	pelvis->AddChild(pelvisLeft);

	Bone* rightUpperLeg = new Bone(XMFLOAT3(0.0f, -5.0f, 0.0f), pelvisRight);
	Bone* rightLowerLeg = new Bone(XMFLOAT3(0.0f, -5.0f, 0.0f), rightUpperLeg);

	pelvisRight->AddChild(rightUpperLeg);
	rightUpperLeg->AddChild(rightLowerLeg);

	Bone* leftUpperLeg = new Bone(XMFLOAT3(-0.0f, -5.0f, 0.0f), pelvisLeft);
	Bone* leftLowerLeg = new Bone(XMFLOAT3(0.0f, -5.0f, 0.0f), leftUpperLeg);


	//leftUpperLeg->SetRotation(XM_PI / 100, 0.0f, 0.0f);

	pelvisLeft->AddChild(leftUpperLeg);
	leftUpperLeg->AddChild(leftLowerLeg);


	leftUpperArm->SetRotation(-0.02f, 0.0f, 0.0f);
	rightUpperArm->SetRotation(-0.02f, 0.0f, 0.0f);

	/*rightLowerArm->SetRotation(-0.02f, 0.0f, 0.0f);	
	leftLowerArm->SetRotation(-0.02f, 0.0f, 0.0f);*/

	leftUpperLeg->SetRotation(-0.02f, 0.0f, 0.0f);
	rightUpperLeg->SetRotation(0.02f, 0.0f, 0.0f);

	leftLowerLeg->SetRotation(0.01f, 0.0f, 0.0f);
	leftLowerLeg->SetCos(false);
	rightLowerLeg->SetRotation(0.01f, 0.0f, 0.0f);
	rightLowerLeg->SetCos(false);
	//_root->Rotate(0.0f, 0.0f, XM_PI / 2);
}

void Skeleton::Draw(ID3D11DeviceContext * pImmediateContext)
{
	//pImmediateContext->UpdateSubresource(_cb_resource, 0, nullptr, &_cb, 0, 0);

	//_root->GetChildren()[0]->GetChildren()[0]->Rotate(0.0f, 0.0f, XM_PI / 200);
	//_root->GetChildren()[0]->GetChildren()[0]->GetChildren()[0]->Rotate(0.0f, 0.0f, XM_PI / 200);

	Bone* navigator;

	// NOTE: We are assuming that the constant buffers and all other draw setup has already taken place
	_cb.World = XMMatrixTranspose(GetWorldMatrix());

	pImmediateContext->UpdateSubresource(_cb_resource, 0, nullptr, &_cb, 0, 0);

	// Set vertex and index buffers
	pImmediateContext->IASetVertexBuffers(0, 1, &_geometry.vertexBuffer, &_geometry.vertexBufferStride, &_geometry.vertexBufferOffset);
	pImmediateContext->IASetIndexBuffer(_geometry.indexBuffer, DXGI_FORMAT_R16_UINT, 0);

	DrawBone(_root, pImmediateContext);

	//pImmediateContext->DrawIndexed(_geometry.numberOfIndices, 0, 0);
	//pImmediateContext->Draw(_geometry.numberOfIndices, 0);

	// do nothing
	time++;
}

void Skeleton::DrawBone(Bone* bone, ID3D11DeviceContext * pImmediateContext)
{	
	bone->Update(time);

	_cb.World = XMMatrixTranspose(GetWorldMatrix() * bone->GetWorldMatrix(XMMatrixIdentity()));
	pImmediateContext->UpdateSubresource(_cb_resource, 0, nullptr, &_cb, 0, 0);

	pImmediateContext->DrawIndexed(_geometry.numberOfIndices, 0, 0);

	vector<Bone*> children = bone->GetChildren();

	if (children.size() > 0)
	{
		for (int i = 0; i < children.size(); ++i)
		{
			DrawBone(children[i], pImmediateContext);
		}
	}

	/*for (auto& child : bone->GetChildren())
	{
		
	}*/
}