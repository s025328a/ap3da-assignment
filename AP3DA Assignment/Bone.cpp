#include "Bone.h"

Bone::Bone(XMFLOAT3 position, Bone* parent)
{
	_startPos = position;
	_parent = parent;
	
	XMMATRIX toParent = XMMatrixTranslation(position.x, position.y, position.z);
	XMStoreFloat4x4(&_toParent, toParent);
}

XMMATRIX Bone::GetWorldMatrix(XMMATRIX& world)
{
	world = world * XMLoadFloat4x4(&_toParent);
	if (_parent != nullptr)
	{
		_parent->GetWorldMatrix(world);
	}
	return world;
}

void Bone::AddChild(Bone* bone)
{
	bone->SetParent(this);
	_children.push_back(bone);
}

void Bone::SetRotation(float x, float y, float z)
{
	_rotation = XMFLOAT3(x, y, z);
}

void Bone::Update(UINT t)
{
	float factor;

	if (_startPos.x >= 0)
	{
		if (_cos)
			factor = cos(t * XM_PI / 200);
		else
			factor = sin(t * XM_PI / 200);
	}
	else
	{
		if (_cos)
			factor = -cos(t * XM_PI / 200);
		else
			factor = -sin(t * XM_PI / 200);
	}

	XMMATRIX r = XMMatrixRotationX(_rotation.x * factor) * XMMatrixRotationY(_rotation.y * factor) * XMMatrixRotationZ(_rotation.z * factor);
	XMMATRIX world = XMLoadFloat4x4(&_toParent) * r;
	XMStoreFloat4x4(&_toParent, world);
}