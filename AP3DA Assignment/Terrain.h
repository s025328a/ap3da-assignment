#ifndef TERRAIN_H
#define TERRAIN_H

#include <vector>
#include "DataTypes.h"
#include "ITerrainGenerator.h"
#include "HeightMapTerrainGenerator.h"
#include "GameObject.h"
#include "DDSTextureLoader.h"

#include <windows.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include "resource.h"
#include "Camera.h"
#include <fstream>

using namespace DirectX;
using namespace std;

class Terrain : public GameObject
{
private:
	//Geometry geometry;
	vector<ID3D11ShaderResourceView*> _textures;

	vector<float>				_heightmap;
	ID3D11ShaderResourceView*	_heightmapRV;

	ID3D11Device* _device;
	//vector<SimpleVertex> vertices;
	//vector < vector<SimpleVertex> > ver;

	int _rows; 
	int _columns;

	XMFLOAT3 _size;
	XMFLOAT3 _position;

	float _width;
	float _depth;

	float _maxWidth;
	float _maxDepth;

	float _minHeight; 
	float _maxHeight;

	float _maxTessPow;

	ITerrainGenerator*			_terrainGen;

	ID3D11Buffer*				_tessBuffer;
	//ID3D11ShaderResourceView*	_heightMap;
	TerrainBuffer			_tb;
	bool _tessellation;

public:
	Terrain();
	~Terrain();

	Terrain(ID3D11Device* device, ITerrainGenerator* terrainGen, bool tessellation);
	void InitData(int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow);
	void InitTerrain();

	float GetHeight(XMFLOAT3 position);

	void AddTexture(const wchar_t* filename);

	void LoadHeightMap(string filename);
	void Draw(ID3D11DeviceContext * pImmediateContext);

	void SetTerrainGenerator(ITerrainGenerator* terrainGen){ _terrainGen = terrainGen; }
};

#endif