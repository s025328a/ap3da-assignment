Texture2D txDiffuse : register(t0);
Texture2D txHeightmap: register(t1);

Texture2D tx0: register(t2);
Texture2D tx1: register(t3);
Texture2D tx2: register(t4);
Texture2D tx3: register(t5);
Texture2D tx4: register(t6);

SamplerState samLinear : register(s0);

SamplerState samHeightmap
{
	Filter = MIN_MAG_LINEAR_MIP_POINT;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------

struct SurfaceInfo
{
	float4 AmbientMtrl;
	float4 DiffuseMtrl;
	float4 SpecularMtrl;
};

struct Light
{
	float4 AmbientLight;
	float4 DiffuseLight;
	float4 SpecularLight;

	float SpecularPower;
	float3 LightVecW;
};

cbuffer ConstantBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;

	SurfaceInfo surface;
	Light light;

	float3 EyePosW;
	float HasTexture;
};

cbuffer TerrainBuffer : register(b1)
{
	float minTessDistance;
	float maxTessDistance;
	float minTessPower;
	float maxTessPower;
	float minTerrainHeight;
	float maxTerrainHeight;
}

//////////////
// TYPEDEFS //
//////////////
struct VS_INPUT
{
	float4 PosL : POSITION;
	float3 NormL : NORMAL;
	float2 Tex : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
	float4 PosH : SV_POSITION;
	float3 NormW : NORMAL;

	float3 PosW : POSITION;
	float2 Tex : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;

	float4 posW = mul(input.PosL, World);
		output.PosW = posW.xyz;

	//output.PosW.y = txHeightmap.SampleLevel(samHeightmap, input.Tex, 0).r;
	//output.PosW.y -= 20.0f;

	output.PosH = mul(posW, View);
	output.PosH = mul(output.PosH, Projection);
	output.Tex = input.Tex;

	float3 normalW = mul(float4(input.NormL, 0.0f), World).xyz;
		output.NormW = normalize(normalW);

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
	float3 normalW = normalize(input.NormW);

	float3 toEye = normalize(EyePosW - input.PosW);

	// Get texture data from file
	float4 textureColour = float4(0.0f, 0.0f, 0.0f, 1.0f);
	if (HasTexture == 1.0f)
	{
		float texValue = 0.0f;
		texValue = (input.PosW.y - minTerrainHeight) / (maxTerrainHeight - minTerrainHeight);

		if (texValue < 0.2f)
		{
			textureColour = tx0.Sample(samLinear, input.Tex * 16.0f);
		}
		else if (texValue < 0.4f)
		{
			textureColour = lerp(tx0.Sample(samLinear, input.Tex * 16.0f), tx1.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.2f) / 0.2f);;
		}
		else if (texValue < 0.6f)
		{
			textureColour = lerp(tx1.Sample(samLinear, input.Tex * 16.0f), tx2.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.4f) / 0.2f);;
		}
		else if (texValue < 0.8f)
		{
			textureColour = lerp(tx2.Sample(samLinear, input.Tex * 16.0f), tx3.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.6f) / 0.2f);;
		}
		else
		{
			textureColour = lerp(tx3.Sample(samLinear, input.Tex * 16.0f), tx4.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.8f) / 0.2f);;
		}
	}
	else
	{
		float texValue = 0.0f;
		texValue = (input.PosW.y - minTerrainHeight) / (maxTerrainHeight - minTerrainHeight);
		textureColour = float4(texValue, texValue, texValue, 1.0f);
		return textureColour;
	}

	float3 ambient = float3(0.0f, 0.0f, 0.0f);
	float3 diffuse = float3(0.0f, 0.0f, 0.0f);
	float3 specular = float3(0.0f, 0.0f, 0.0f);

	float3 lightLecNorm = normalize(light.LightVecW);
	// Compute Colour

	// Compute the reflection vector.
	float3 r = reflect(-lightLecNorm, normalW);

	// Determine how much specular light makes it into the eye.
	float specularAmount = pow(max(dot(r, toEye), 0.0f), light.SpecularPower);

	// Determine the diffuse light intensity that strikes the vertex.
	float diffuseAmount = max(dot(lightLecNorm, normalW), 0.0f);

	// Only display specular when there is diffuse
	if (diffuseAmount <= 0.0f)
	{
		specularAmount = 0.0f;
	}

	// Compute the ambient, diffuse, and specular terms separately.
	specular += specularAmount * (surface.SpecularMtrl * light.SpecularLight).rgb;
	diffuse += diffuseAmount * (surface.DiffuseMtrl * light.DiffuseLight).rgb;
	ambient += (surface.AmbientMtrl * light.AmbientLight).rgb;

	// Sum all the terms together and copy over the diffuse alpha.
	float4 finalColour;

	if (HasTexture == 1.0f)
	{
		finalColour.rgb = (textureColour.rgb * (ambient + diffuse)) + specular;
	}
	else
	{
		finalColour.rgb = ambient + diffuse + specular;
	}

	finalColour.a = surface.DiffuseMtrl.a;

	return finalColour;
}

////--------------------------------------------------------------------------------------
//// Vertex Shader
////--------------------------------------------------------------------------------------
//VS_OUTPUT VS(VERTEX_INPUT input)
//{
//	VS_OUTPUT output = (VS_OUTPUT)0;
//
//	float4 posW = mul(input.PosL, World);
//		output.PosW = posW.xyz;
//
//	/*float y = txHeightmap.SampleLevel(samHeightmap, input.Tex, 0).r;
//	output.PosW.y = y;*/
//
//	output.PosH = mul(posW, View);
//	output.PosH = mul(output.PosH, Projection);
//	output.Tex = input.Tex;
//
//	float3 normalW = mul(float4(input.NormL, 0.0f), World).xyz;
//		output.NormW = normalize(normalW);
//
//	return output;
//}
//
////--------------------------------------------------------------------------------------
//// Pixel Shader
////--------------------------------------------------------------------------------------
//float4 PS(VS_OUTPUT input) : SV_TARGET
//{
//	float3 normalW = normalize(input.NormW);
//
//	float3 toEye = normalize(EyePosW - input.PosW);
//
//	// Get texture data from file
//
//	float4 textureColour = float4(1.0f, 0.0f, 0.0f, 1.0f);
//
//	/*float texValue = 0.0f;
//	texValue = (input.PosW.y - minTerrainHeight) / (maxTerrainHeight - minTerrainHeight);
//
//	if (texValue < 0.2f)
//	{
//		textureColour = tx0.Sample(samLinear, input.Tex * 16.0f);
//	}
//	else if (texValue < 0.4f)
//	{
//		textureColour = lerp(tx0.Sample(samLinear, input.Tex * 16.0f), tx1.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.2f) / 0.2f);;
//	}
//	else if (texValue < 0.6f)
//	{
//		textureColour = lerp(tx1.Sample(samLinear, input.Tex * 16.0f), tx2.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.4f) / 0.2f);;
//	}
//	else if (texValue < 0.8f)
//	{
//		textureColour = lerp(tx2.Sample(samLinear, input.Tex * 16.0f), tx3.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.6f) / 0.2f);;
//	}
//	else
//	{
//		textureColour = lerp(tx3.Sample(samLinear, input.Tex * 16.0f), tx4.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.8f) / 0.2f);;
//	}*/
//
//	//float4 textureColour = float4(1.0f, 1.0f, 1.0f, 1.0f);
//
//	float3 ambient = float3(0.0f, 0.0f, 0.0f);
//	float3 diffuse = float3(0.0f, 0.0f, 0.0f);
//	float3 specular = float3(0.0f, 0.0f, 0.0f);
//
//	float3 lightLecNorm = normalize(light.LightVecW);
//	// Compute Colour
//
//	// Compute the reflection vector.
//	float3 r = reflect(-lightLecNorm, normalW);
//
//	// Determine how much specular light makes it into the eye.
//	float specularAmount = pow(max(dot(r, toEye), 0.0f), light.SpecularPower);
//
//	// Determine the diffuse light intensity that strikes the vertex.
//	float diffuseAmount = max(dot(lightLecNorm, normalW), 0.0f);
//
//	// Only display specular when there is diffuse
//	if (diffuseAmount <= 0.0f)
//	{
//		specularAmount = 0.0f;
//	}
//
//	// Compute the ambient, diffuse, and specular terms separately.
//	specular += specularAmount * (surface.SpecularMtrl * light.SpecularLight).rgb;
//	diffuse += diffuseAmount * (surface.DiffuseMtrl * light.DiffuseLight).rgb;
//	ambient += (surface.AmbientMtrl * light.AmbientLight).rgb;
//
//	// Sum all the terms together and copy over the diffuse alpha.
//	float4 finalColour;
//
//	if (HasTexture == 1.0f)
//	{
//		finalColour.rgb = (textureColour.rgb * (ambient + diffuse)) + specular;
//	}
//	else
//	{
//		finalColour.rgb = ambient + diffuse + specular;
//	}
//
//	finalColour.a = surface.DiffuseMtrl.a;
//
//	return finalColour;
//
//	//return txDiffuse.Sample(samLinear, input.Tex);
//
//	////return input.color;
//	//return float4(1.0f, 0.0f, 0.0f, 1.0f);
//}

technique11 ColorTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}