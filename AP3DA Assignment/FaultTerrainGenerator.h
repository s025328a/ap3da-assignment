#ifndef FAULT_T_GENERATOR
#define FAULT_T_GENERATOR

#include "ITerrainGenerator.h"
#include <fstream>
#include <string>

class FaultTerrainGenerator : public ITerrainGenerator
{
private:
	float _displacement = 1.0f;
	int _iterations;

	int _width;
	int _height;

public:
	FaultTerrainGenerator(int iterations) : _iterations(iterations){}
	~FaultTerrainGenerator();

	Geometry GenerateTerrain(ID3D11Device* device, int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow);
	ID3D11ShaderResourceView* GetShaderResourceView(ID3D11Device* device);
};

#endif