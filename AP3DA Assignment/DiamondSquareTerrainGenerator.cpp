#include "DiamondSquareTerrainGenerator.h"

Geometry DiamondSquareTerrainGenerator::GenerateTerrain(ID3D11Device* device, int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow)
{
	HRESULT hr;
	Geometry geometry;
	//vector<SimpleVertex> vertices;

	float width = size.x;
	float depth = size.z;
	float minHeight = position.y;
	float maxHeight = position.y + size.y;

	XMFLOAT3 scale = XMFLOAT3(width / (float)rows, maxHeight - minHeight, depth / (float)columns);
	float yOffset = minHeight;

	float maxTess = pow(2.0f, maxTessPow);

	int maxRows = (maxTess * rows);
	int maxCols = (maxTess * columns);

	int vertex_total = (rows + 1) * (columns + 1);
	int vertex_width = rows + 1;
	int vertex_height = columns + 1;

	_positions.resize((maxRows + 1) * (maxCols + 1));
	_heightmap.resize((maxRows + 1) * (maxCols + 1));

	XMFLOAT2 tex;

	float min_height = 0.0f;
	float max_height = 0.0f;

	for (int i = 0; i < vertex_width; i++)
	{
		float x = position.x + i * scale.x/* - (width / 2.0f)*/;
		tex.x = (float)i / rows;

		for (int j = 0; j < vertex_height; j++)
		{
			tex.y = (float)j / columns;
			vertices.push_back({ XMFLOAT3(x, 0.0f, position.z - (j * scale.z/* - (depth / 2.0f)*/)), XMFLOAT3(0.0f, 1.0f, 0.0f), tex });
		}
	}

	vertex_total = (maxRows + 1) * (maxCols + 1);
	vertex_width = maxRows + 1;
	vertex_height = maxCols + 1;

	_width = vertex_width;
	_height = vertex_height;

	scale = XMFLOAT3(width / (float)maxRows, maxHeight - minHeight, depth / (float)maxCols);

	for (int i = 0; i < vertex_width; i++)
	{
		float x = position.x + i * scale.x/* - (width / 2.0f)*/;
		for (int j = 0; j < vertex_height; j++)
		{
			_positions[i*vertex_height + j] = (XMFLOAT3(x, 0.0f, position.z - (j * scale.z/* - (depth / 2.0f)*/)));
		}
	}

	std::vector<unsigned char> in(vertex_total);

	// Open the file.
	std::ifstream inFile;
	string _filename = "Heightmaps\\ds";
	//_filename.append(std::to_string(_iterations));
	_filename.append(std::to_string(maxRows));
	_filename.append(std::to_string(maxCols));
	_filename.append(".RAW");

	inFile.open(_filename.c_str(), std::ios_base::binary);

	if (inFile)
	{
		// Read the RAW bytes.
		inFile.read((char*)&in[0], (std::streamsize)in.size());
		// Done with file.
		inFile.close();

		for (int i = 0; i < vertex_total; i++)
		{
			_heightmap[i] = (maxHeight - minHeight) * (float)in[i] / 255.0f + position.y;
		}
	}
	else
	{
		float displacement = (float)maxRows;
		int side = maxRows;
				
		while (side > 1)
		{
			for (int i = 0; i < maxCols / side; ++i)
			{
				for (int j = 0; j < maxRows / side; ++j)
				{
					XMFLOAT3 positions[4];

					//int index = (j * side * (maxRows + 1)) + (i * side);

					int index = GetIndex(maxRows + 1, maxCols + 1, i * side, j * side);

					positions[0] = _positions[index];
					positions[1] = _positions[index + side];
					positions[2] = _positions[index + (side * (maxCols + 1))];
					positions[3] = _positions[index + (side * (maxCols + 1)) + side];

					XMFLOAT3* centre = &_positions[index + (side * (maxCols + 1) / 2) + side / 2];

					centre->y = (positions[0].y + positions[1].y + positions[2].y + positions[3].y) / 4.0f;
					centre->y += (((float)(rand() % 2000) / 1000.0f) - 1.0f) * displacement;
				}
			}
			
			//diamond stage
			for (int i = 0; i <= maxCols / side; ++i)
			{
				for (int j = 0; j <= maxRows / side; ++j)
				{
					//right, then down

					int index = GetIndex(maxRows + 1, maxCols + 1, i * side, j * side);

					//right
					if (i < maxCols / side)
					{
						XMFLOAT3* centre = &_positions[index + (side * (maxCols + 1) / 2)];

						vector<XMFLOAT3> positions;

						positions.push_back(_positions[index]);
						positions.push_back(_positions[index + side * (maxCols + 1)]);

						if (j > 0)
						{
							positions.push_back(_positions[index + (side * (maxCols + 1) / 2) - (side / 2)]);
						}
						if (j < maxRows / side)
						{
							positions.push_back(_positions[index + (side * (maxCols + 1) / 2) + (side / 2)]);
						}

						float average = 0.0f;

						for (int k = 0; k < positions.size(); ++k)
						{
							average += positions[k].y;
						}
						average /= (float)positions.size();

						centre->y = average + (((float)(rand() % 2000) / 1000.0f) - 1.0f) * displacement;
						int a = 1;
					}

					//down
					if (j < maxRows / side)
					{
						XMFLOAT3* centre = &_positions[index + (side / 2)];

						vector<XMFLOAT3> positions;

						positions.push_back(_positions[index]);
						positions.push_back(_positions[index + side]);

						if (i > 0)
						{
							positions.push_back(_positions[index - (side * (maxCols + 1) / 2) + (side / 2)]);
						}
						if (i < maxCols / side)
						{
							positions.push_back(_positions[index + (side * (maxCols + 1) / 2) + (side / 2)]);
						}

						float average = 0.0f;

						for (int k = 0; k < positions.size(); ++k)
						{
							average += positions[k].y;
						}
						average /= (float)positions.size();

						centre->y = average + (((float)(rand() % 2000) / 1000.0f) -1.0f) * displacement;
						int a = 1;
					}
				}
			}

			//if (side < 64)
			//{
			//	side = 0;
			//}

			side /= 2;
			displacement /= 2;
		}
		
		for (int i = 0; i < _positions.size(); ++i)
		{
			XMFLOAT3* pos = &_positions[i];
			if (pos->y > max_height)
			{
				max_height = pos->y;
			}
			else if (pos->y < min_height)
			{
				min_height = pos->y;
			}
		}
			

		float heightScale = (maxHeight - minHeight) / (max_height - min_height);
		float heightDisplacement = minHeight - (heightScale * min_height);

		for (int i = 0; i < vertex_total; i++)
		{
			XMFLOAT3 pos = _positions[i];
			pos.y = (pos.y * heightScale) + heightDisplacement;
			_positions[i] = pos;
		}

		std::ofstream outFile(_filename.c_str(), std::ios_base::out | std::ios_base::binary);
		std::vector<unsigned char> out(vertex_total);

		for (int i = 0; i < vertex_total; i++)
		{
			_heightmap[i] = _positions[i].y;
			out[i] = 255.0f * (_positions[i].y - position.y) / size.y;
		}

		outFile.write((char*)&out[0], vertex_total);
	}

	if (!(maxTessPow > 0.0f))
	{
		for (int i = 0; i < vertices.size(); ++i)
		{
			vertices[i].PosL.y = _heightmap[i];
		}
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * (rows + 1) * (columns + 1);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices[0];

	hr = device->CreateBuffer(&bd, &InitData, &geometry.vertexBuffer);

	geometry.vertexBufferOffset = 0;
	geometry.vertexBufferStride = sizeof(SimpleVertex);

	std::vector<WORD> indices;

	geometry.numberOfIndices = 0;

	vertex_width = rows + 1;
	vertex_height = columns + 1;

	if (maxTessPow > 0.0f)
	{
		for (int i = 0; i < rows; i++)
		{
			int start = i * vertex_width;
			for (int j = 0; j < columns; j++)
			{
				indices.push_back(start + j);
				indices.push_back(start + j + vertex_width);
				indices.push_back(start + j + 1);
				indices.push_back(start + j + vertex_width + 1);
				geometry.numberOfIndices += 4;
			}
		}
	}
	else
	{
		for (int i = 0; i < rows; i++)
		{
			int start = i * vertex_height;
			for (int j = 0; j < columns; j++)
			{
				indices.push_back(start + j);
				indices.push_back(start + j + vertex_height);
				indices.push_back(start + j + vertex_height + 1);
				indices.push_back(start + j);
				indices.push_back(start + j + vertex_height + 1);
				indices.push_back(start + j + 1);
				geometry.numberOfIndices += 6;
			}
		}
	}

	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * geometry.numberOfIndices;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	//D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &indices[0];
	hr = device->CreateBuffer(&bd, &InitData, &geometry.indexBuffer);

	return geometry;
}

ID3D11ShaderResourceView* DiamondSquareTerrainGenerator::GetShaderResourceView(ID3D11Device* device)
{
	ID3D11ShaderResourceView* resource;
	HRESULT hr;

	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = _width;
	texDesc.Height = _height;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R32_FLOAT;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;

	// HALF is defined in xnamath.h, for storing 16-bit float.
	//std::vector<HALF> hmap(mHeightmap.size());
	//std::transform(mHeightmap.begin(), mHeightmap.end(),
	//	hmap.begin(), XMConvertFloatToHalf);

	D3D11_SUBRESOURCE_DATA data;
	data.pSysMem = &_heightmap[0];
	data.SysMemPitch = _width*sizeof(float);
	data.SysMemSlicePitch = 0;
	ID3D11Texture2D* hmapTex = 0;

	hr = device->CreateTexture2D(&texDesc, &data, &hmapTex);

	if (FAILED(hr))
		return NULL;

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = -1;
	hr = device->CreateShaderResourceView(
		hmapTex, &srvDesc, &resource);

	if (FAILED(hr))
		return NULL;

	// SRV saves reference.
	hmapTex->Release();

	return resource;
}