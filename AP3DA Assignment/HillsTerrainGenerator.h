#ifndef HILL_TERRAIN_GENERATOR
#define HILL_TERRAIN_GENERATOR

#include "ITerrainGenerator.h"
#include <fstream>
#include <string>

class HillsTerrainGenerator : public ITerrainGenerator
{
private:
	/*float _displacement;*/
	int _iterations;

	int _width;
	int _height;

	float _min_radius;
	float _max_radius;

public:
	HillsTerrainGenerator(int iterations, float minRadius, float maxRadius) : _iterations(iterations), _min_radius(minRadius), _max_radius(maxRadius){}
	~HillsTerrainGenerator();

	Geometry GenerateTerrain(ID3D11Device* device, int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow);
	ID3D11ShaderResourceView* GetShaderResourceView(ID3D11Device* device);
};

#endif