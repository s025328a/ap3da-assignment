#ifndef ITERRAIN_GEN_H
#define ITERRAIN_GEN_H

#include <vector>
#include "DataTypes.h"

#include <windows.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include "resource.h"
#include "Camera.h"
#include <fstream>

using namespace DirectX;
using namespace std;

class ITerrainGenerator
{
protected:
	vector<SimpleVertex> vertices;
	int vertex_width;
	int vertex_height;

	vector<XMFLOAT3> _positions;
	vector<float> _heightmap;

	int GetIndex(int vertex_width, int vertex_height, int x, int y)
	{
		int index = (x * vertex_height) + y;

		if (index < vertex_width * vertex_height && index >= 0)
			return index;
		else
			return -1;
	}

public:
	virtual Geometry GenerateTerrain(ID3D11Device* device, int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow) = 0;
	virtual ID3D11ShaderResourceView* GetShaderResourceView(ID3D11Device* device){ return NULL; }
	vector<float> GetTessellatedHeightmap(){ return _heightmap; }
	//{
	//	HRESULT hr;
	//	Geometry geometry;

	//	XMFLOAT3 scale = XMFLOAT3(width / (float)rows, maxHeight - minHeight, depth / (float)columns);
	//	float yOffset = minHeight;

	//	int vertex_total = (rows + 1) * (columns + 1);
	//	int vertex_width = rows + 1;
	//	int vertex_height = columns + 1;

	//	XMFLOAT2 tex;
	//			
	//	for (int i = 0; i < vertex_width; i++)
	//	{
	//		float x = i * scale.x - (width / 2.0f);
	//		tex.x = (float)i / rows;

	//		for (int j = 0; j < vertex_height; j++)
	//		{
	//			tex.y = (float)j / columns;
	//			vertices.push_back({ XMFLOAT3(x, 0.0f, -(j * scale.z - (depth / 2.0f))), XMFLOAT3(0.0f, 1.0f, 0.0f), tex });
	//		}
	//	}

	//	D3D11_BUFFER_DESC bd;
	//	ZeroMemory(&bd, sizeof(bd));
	//	bd.Usage = D3D11_USAGE_DEFAULT;
	//	bd.ByteWidth = sizeof(SimpleVertex) * vertex_total;
	//	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	//	bd.CPUAccessFlags = 0;

	//	D3D11_SUBRESOURCE_DATA InitData;
	//	ZeroMemory(&InitData, sizeof(InitData));
	//	InitData.pSysMem = &vertices[0];

	//	hr = device->CreateBuffer(&bd, &InitData, &geometry.vertexBuffer);

	//	geometry.vertexBufferOffset = 0;
	//	geometry.vertexBufferStride = sizeof(SimpleVertex);

	//	std::vector<WORD> indices;

	//	geometry.numberOfIndices = 0;

	//	for (int i = 0; i < rows; i++)
	//	{
	//		int start = i * vertex_height;
	//		for (int j = 0; j < columns; j++)
	//		{
	//			indices.push_back(start + j);
	//			indices.push_back(start + j + vertex_height);
	//			indices.push_back(start + j + vertex_height + 1);
	//			indices.push_back(start + j);
	//			indices.push_back(start + j + vertex_height + 1);
	//			indices.push_back(start + j + 1);
	//			geometry.numberOfIndices += 6;
	//		}
	//	}

	//	ZeroMemory(&bd, sizeof(bd));

	//	bd.Usage = D3D11_USAGE_DEFAULT;
	//	bd.ByteWidth = sizeof(WORD) * geometry.numberOfIndices;
	//	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	//	bd.CPUAccessFlags = 0;

	//	//D3D11_SUBRESOURCE_DATA InitData;
	//	ZeroMemory(&InitData, sizeof(InitData));
	//	InitData.pSysMem = &indices[0];
	//	hr = device->CreateBuffer(&bd, &InitData, &geometry.indexBuffer);

	//	return geometry;
	//}
};

#endif