Texture2D txDiffuse : register(t0);
Texture2D txHeightmap: register(t1);

Texture2D tx0: register(t2);
Texture2D tx1: register(t3);
Texture2D tx2: register(t4);
Texture2D tx3: register(t5);
Texture2D tx4: register(t6);

SamplerState samLinear : register(s0);

SamplerState samHeightmap
{
	Filter = MIN_MAG_LINEAR_MIP_POINT;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------

struct SurfaceInfo
{
	float4 AmbientMtrl;
	float4 DiffuseMtrl;
	float4 SpecularMtrl;
};

struct Light
{
	float4 AmbientLight;
	float4 DiffuseLight;
	float4 SpecularLight;

	float SpecularPower;
	float3 LightVecW;
};

cbuffer ConstantBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;

	SurfaceInfo surface;
	Light light;

	float3 EyePosW;
	float HasTexture;
}

cbuffer TerrainBuffer : register(b1)
{
	float minTessDistance;
	float maxTessDistance;
	float minTessPower;
	float maxTessPower;
	float minTerrainHeight;
	float maxTerrainHeight;
}

//////////////
// TYPEDEFS //
//////////////
struct VERTEX_INPUT
{
	float3 PosL : POSITION;
	float3 NormL : NORMAL;

	float2 Tex : TEXCOORD0;
	//float4 color : COLOR;
};

struct HULL_INPUT
{
	float3 PosW : POSITION;
	//float4 color : COLOR;

	float3 NormW : NORMAL;
	float2 Tex : TEXCOORD0;
};

struct PatchTess
{
	float edges[4] : SV_TessFactor;
	float inside[2] : SV_InsideTessFactor;
};

struct HULL_OUTPUT
{
	float3 PosW : POSITION;
	//float4 color : COLOR;

	float3 NormW : NORMAL;
	float2 Tex : TEXCOORD0;
};

struct PIXEL_INPUT
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	//float4 color : COLOR;

	float3 NormW : NORMAL;
	float2 Tex : TEXCOORD0;
	int TexIndex : TEXCOORD1;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
HULL_INPUT VS(VERTEX_INPUT input)
{
	HULL_INPUT output;
	
	output.PosW = mul(input.PosL, World);
	output.NormW = mul(input.NormL, World);

	//output.color = input.color;
	output.Tex = input.Tex;

	return output;
}

float TessFactor(float3 position)
{
	/*float min = 125.0f;
	float max = 350.0f;*/
	
	position.y = 0.0f;

	float d = distance(position, float3(EyePosW.x, 0.0f, EyePosW.z));

	float s = saturate((d - minTessDistance) / (maxTessDistance - minTessDistance));

	float accuracy = 1.0f;

	//return floor(accuracy * pow(2, (lerp(maxTessPower, minTessPower, s)))) / accuracy;
	return lerp((float)pow(2, maxTessPower), 1.0f, s);
	//return pow(2, (lerp(4.0f, 1.0f, s)));
}

////////////////////////////////////////////////////////////////////////////////
// Patch Constant Function
////////////////////////////////////////////////////////////////////////////////
PatchTess ConstantHS(InputPatch<HULL_INPUT, 4> patch, uint patchId : SV_PrimitiveID)
{
	PatchTess output;

	float tessellationAmount = 64.0f;
	
	float3 edge0 = 0.5f*(patch[0].PosW + patch[2].PosW);
	float3 edge1 = 0.5f*(patch[0].PosW + patch[1].PosW);
	float3 edge2 = 0.5f*(patch[1].PosW + patch[3].PosW);
	float3 edge3 = 0.5f*(patch[2].PosW + patch[3].PosW);
	float3 c = 0.25f*(patch[0].PosW + patch[1].PosW + patch[2].PosW + patch[3].PosW);

	float tess = TessFactor(c);

	// Set the tessellation factors for the three edges of the triangle.
	output.edges[0] = TessFactor(edge0);
	output.edges[1] = TessFactor(edge1);
	output.edges[2] = TessFactor(edge2);
	output.edges[3] = TessFactor(edge3);

	// Set the tessellation factor for tessallating inside the triangle.
	output.inside[0] = tess;
	output.inside[1] = tess;

	return output;
}


////////////////////////////////////////////////////////////////////////////////
// Hull Shader
////////////////////////////////////////////////////////////////////////////////
[domain("quad")]
[partitioning("fractional_even")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("ConstantHS")]

HULL_OUTPUT HS(InputPatch<HULL_INPUT, 4> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
	HULL_OUTPUT output;

	// Set the position for this control point as the output position.
	output.PosW = patch[pointId].PosW;

	if (pointId == 0)
	{
		float3 normal = cross(patch[1].PosW - patch[0].PosW, patch[2].PosW - patch[0].PosW);
		output.NormW = normal;
	}
	else
	{
		output.NormW = patch[pointId].NormW;
	}

	output.Tex = patch[pointId].Tex;

	// Set the input color as the output color.
	//output.color = patch[pointId].color;

	return output;
}


////////////////////////////////////////////////////////////////////////////////
// Domain Shader
////////////////////////////////////////////////////////////////////////////////
[domain("quad")]

PIXEL_INPUT DS(PatchTess input, float2 uvwCoord : SV_DomainLocation, const OutputPatch<HULL_OUTPUT, 4> patch)
{
	PIXEL_INPUT output;

	// Determine the position of the new vertex.
	float3 v1 = lerp(patch[0].PosW, patch[1].PosW, uvwCoord.x);
	float3 v2 = lerp(patch[2].PosW, patch[3].PosW, uvwCoord.x);
	output.PosW = lerp(v1, v2, uvwCoord.y);

	float2 t1 = lerp(patch[0].Tex, patch[1].Tex, uvwCoord.x);
	float2 t2 = lerp(patch[2].Tex, patch[3].Tex, uvwCoord.x);
	output.Tex = lerp(t1, t2, uvwCoord.y);
	
	//output.PosW.y = txDiffuse.Sample(samLinear, output.Tex).x;
	//float4 test = txDiffuse.SampleLevel(samLinear, output.Tex, 0);
	/*float4 pos;
	pos.y = test.x;*/
	
	float y = txHeightmap.SampleLevel(samHeightmap, output.Tex, 0).r;

	output.PosW.y = y;

	/*y = y * 25.0f - 20.0f;*/

	output.PosH = float4(output.PosW, 1.0f);
	//output.PosH = float4(output.PosW, 1.0f);

	// Calculate the position of the new vertex against the world, view, and projection matrices.
	output.PosH = mul(output.PosH, View);
	//output.position = mul(output.position, View);
	output.PosH = mul(output.PosH, Projection);

	float3 n1 = lerp(patch[0].NormW, patch[1].NormW, uvwCoord.x);
	float3 n2 = lerp(patch[2].NormW, patch[3].NormW, uvwCoord.x);
	output.NormW = lerp(n1, n2, uvwCoord.y);
	

	// Send the input color into the pixel shader.
	//output.color = patch[0].color;

	return output;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 PS(PIXEL_INPUT input) : SV_TARGET
{
	float3 normalW = normalize(input.NormW);

	float3 toEye = normalize(EyePosW - input.PosW);

	// Get texture data from file

	float4 textureColour = float4(0.0f, 0.0f, 0.0f, 1.0f);

	float texValue = 0.0f;
	texValue = (input.PosW.y - minTerrainHeight) / (maxTerrainHeight - minTerrainHeight);

	if (texValue < 0.2f)
	{
		textureColour = tx0.Sample(samLinear, input.Tex * 16.0f);
	}
	else if (texValue < 0.4f)
	{
		textureColour = lerp(tx0.Sample(samLinear, input.Tex * 16.0f), tx1.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.2f) / 0.2f);;
	}
	else if (texValue < 0.6f)
	{
		textureColour = lerp(tx1.Sample(samLinear, input.Tex * 16.0f), tx2.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.4f) / 0.2f);;
	}
	else if (texValue < 0.8f)
	{
		textureColour = lerp(tx2.Sample(samLinear, input.Tex * 16.0f), tx3.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.6f) / 0.2f);;
	}
	else
	{
		textureColour = lerp(tx3.Sample(samLinear, input.Tex * 16.0f), tx4.Sample(samLinear, input.Tex * 16.0f), (texValue - 0.8f) / 0.2f);;
	}

	//float4 textureColour = float4(1.0f, 1.0f, 1.0f, 1.0f);

	float3 ambient = float3(0.0f, 0.0f, 0.0f);
	float3 diffuse = float3(0.0f, 0.0f, 0.0f);
	float3 specular = float3(0.0f, 0.0f, 0.0f);

	float3 lightLecNorm = normalize(light.LightVecW);
	// Compute Colour

	// Compute the reflection vector.
	float3 r = reflect(-lightLecNorm, normalW);

	// Determine how much specular light makes it into the eye.
	float specularAmount = pow(max(dot(r, toEye), 0.0f), light.SpecularPower);

	// Determine the diffuse light intensity that strikes the vertex.
	float diffuseAmount = max(dot(lightLecNorm, normalW), 0.0f);

	// Only display specular when there is diffuse
	if (diffuseAmount <= 0.0f)
	{
		specularAmount = 0.0f;
	}

	// Compute the ambient, diffuse, and specular terms separately.
	specular += specularAmount * (surface.SpecularMtrl * light.SpecularLight).rgb;
	diffuse += diffuseAmount * (surface.DiffuseMtrl * light.DiffuseLight).rgb;
	ambient += (surface.AmbientMtrl * light.AmbientLight).rgb;

	// Sum all the terms together and copy over the diffuse alpha.
	float4 finalColour;

	if (HasTexture == 1.0f)
	{
		finalColour.rgb = (textureColour.rgb * (ambient + diffuse)) + specular;
	}
	else
	{
		finalColour.rgb = ambient + diffuse + specular;
	}

	finalColour.a = surface.DiffuseMtrl.a;

	return finalColour;

	//return txDiffuse.Sample(samLinear, input.Tex);

	////return input.color;
	//return float4(1.0f, 0.0f, 0.0f, 1.0f);
}