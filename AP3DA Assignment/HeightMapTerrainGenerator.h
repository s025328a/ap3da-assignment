#ifndef HM_TERRAIN_GENERATOR
#define HM_TERRAIN_GENERATOR

#include <vector>
#include "DataTypes.h"
#include "ITerrainGenerator.h"
#include <fstream>

using namespace DirectX;

class HeightMapTerrainGenerator : public ITerrainGenerator
{
public:
	HeightMapTerrainGenerator(){};
	HeightMapTerrainGenerator(string filename, int width, int height);
	Geometry GenerateTerrain(ID3D11Device* device, int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow);
	ID3D11ShaderResourceView* GetShaderResourceView(ID3D11Device* device);

private:
	string _filename;
	int _width;
	int _height;

	std::vector<float> mHeightmap;
};


#endif