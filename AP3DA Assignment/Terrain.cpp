#include "Terrain.h"

//Terrain::Terrain(int rows, int columns, float scale, ID3D11Device* device)
//{
//	HRESULT hr;
//
//	XMFLOAT2 half = XMFLOAT2((float)(rows + 1) * scale / 2.0f, (float)(columns + 1) * scale / 2.0f);
//	int vertex_total = (rows + 1) * (columns + 1);
//	int vertex_width = rows + 1;
//	int vertex_height = columns + 1;
//
//	XMFLOAT2 tex;
//
//	// A height for each vertex 
//	std::vector<unsigned char> in(vertex_width * vertex_height);
//
//	std::vector<float> mHeightmap;
//	float heightScale = 50.0f * scale;
//
//	// Open the file.
//	std::ifstream inFile;
//	string filename = "terrain.RAW";
//	inFile.open(filename.c_str(), std::ios_base::binary);
//
//	if (inFile)
//	{
//		// Read the RAW bytes.
//		inFile.read((char*)&in[0], (std::streamsize)in.size());
//		// Done with file.
//		inFile.close();
//	}
//
//	// Copy the array data into a float array and scale it. 
//	mHeightmap.resize(vertex_width * vertex_height, 0);
//
//	for (UINT i = 0; i < vertex_width * vertex_height; ++i)
//	{
//		mHeightmap[i] = (in[i] / 255.0f) * heightScale;
//	}
//
//	float y;
//	//float maxHeight = 0.25f;
//
//	for (int i = 0; i < vertex_width; i++)
//	{
//		float x = i * scale - half.x;
//		tex.x = (float)i / rows;
//
//		for (int j = 0; j < vertex_height; j++)
//		{
//			y = mHeightmap[i + j];
//			tex.y = (float)j / columns;
//			vertices.push_back({ XMFLOAT3(x, y, -(j * scale - half.y)), XMFLOAT3(0.0f, 1.0f, 0.0f), tex });
//		}
//	}
//
//	D3D11_BUFFER_DESC bd;
//	ZeroMemory(&bd, sizeof(bd));
//	bd.Usage = D3D11_USAGE_DEFAULT;
//	bd.ByteWidth = sizeof(SimpleVertex) * vertex_total;
//	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	bd.CPUAccessFlags = 0;
//
//	D3D11_SUBRESOURCE_DATA InitData;
//	ZeroMemory(&InitData, sizeof(InitData));
//	InitData.pSysMem = &vertices[0];
//
//	hr = device->CreateBuffer(&bd, &InitData, &geometry.vertexBuffer);
//
//	geometry.vertexBufferOffset = 0;
//	geometry.vertexBufferStride = sizeof(SimpleVertex);
//
//	std::vector<WORD> indices;
//
//	geometry.numberOfIndices = 0;
//
//	for (int i = 0; i < rows; i++)
//	{
//		int start = i * vertex_height;
//		for (int j = 0; j < columns; j++)
//		{
//			indices.push_back(start + j);
//			indices.push_back(start + j + vertex_height);
//			indices.push_back(start + j + vertex_height + 1);
//			indices.push_back(start + j);
//			indices.push_back(start + j + vertex_height + 1);
//			indices.push_back(start + j + 1);
//			geometry.numberOfIndices += 6;
//		}
//	}
//
//	ZeroMemory(&bd, sizeof(bd));
//
//	bd.Usage = D3D11_USAGE_DEFAULT;
//	bd.ByteWidth = sizeof(WORD) * geometry.numberOfIndices;
//	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	bd.CPUAccessFlags = 0;
//
//	//D3D11_SUBRESOURCE_DATA InitData;
//	ZeroMemory(&InitData, sizeof(InitData));
//	InitData.pSysMem = &indices[0];
//	hr = device->CreateBuffer(&bd, &InitData, &geometry.indexBuffer);
//}

Terrain::Terrain()
{

}

Terrain::Terrain(ID3D11Device* device, ITerrainGenerator* terrainGen, bool tessellation)
{
	_device = device;
	_terrainGen = terrainGen;
	_tessellation = tessellation;

	_parent = nullptr;
	_position = XMFLOAT3();
	_rotation = XMFLOAT3();
	_scale = XMFLOAT3(1.0f, 1.0f, 1.0f);

	_textureRV = nullptr;

	HRESULT hr;

	// Create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(TerrainBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = device->CreateBuffer(&bd, nullptr, &_tessBuffer);



	/*if (FAILED(hr))
		return hr;*/
}

void Terrain::InitData(int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow)
{
	_rows = rows;
	_columns = columns;
	_size = size;
	_position = position;
	_maxTessPow = maxTessPow;

	_maxWidth = (_rows * pow(2.0f, _maxTessPow)) + 1;
	_maxDepth= (_columns * pow(2.0f, _maxTessPow)) + 1;
}

void Terrain::InitTerrain()
{
	_geometry = _terrainGen->GenerateTerrain(_device, _rows, _columns, _size, _position, _maxTessPow);

	_heightmap = _terrainGen->GetTessellatedHeightmap();
	_heightmapRV = _terrainGen->GetShaderResourceView(_device);

	//ID3D11ShaderResourceView* resource;
	//HRESULT hr;

	//D3D11_TEXTURE2D_DESC texDesc;
	//texDesc.Width = (_rows * pow(2.0f, _maxTessPow)) + 1;
	//texDesc.Height = (_columns * pow(2.0f, _maxTessPow)) + 1;
	//texDesc.MipLevels = 1;
	//texDesc.ArraySize = 1;
	//texDesc.Format = DXGI_FORMAT_R32_FLOAT;
	//texDesc.SampleDesc.Count = 1;
	//texDesc.SampleDesc.Quality = 0;
	//texDesc.Usage = D3D11_USAGE_DEFAULT;
	//texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	//texDesc.CPUAccessFlags = 0;
	//texDesc.MiscFlags = 0;

	//// HALF is defined in xnamath.h, for storing 16-bit float.
	////std::vector<HALF> hmap(mHeightmap.size());
	////std::transform(mHeightmap.begin(), mHeightmap.end(),
	////	hmap.begin(), XMConvertFloatToHalf);

	//D3D11_SUBRESOURCE_DATA data;
	//data.pSysMem = &_heightmap[0];
	//data.SysMemPitch = _width*sizeof(float);
	//data.SysMemSlicePitch = 0;
	//ID3D11Texture2D* hmapTex = 0;

	//hr = _device->CreateTexture2D(&texDesc, &data, &hmapTex);

	//if (FAILED(hr))
	//	return;

	//D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	//srvDesc.Format = texDesc.Format;
	//srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	//srvDesc.Texture2D.MostDetailedMip = 0;
	//srvDesc.Texture2D.MipLevels = -1;
	//hr = _device->CreateShaderResourceView(
	//	hmapTex, &srvDesc, &_heightmapRV);

	//// SRV saves reference.
	//hmapTex->Release();
}

float Terrain::GetHeight(XMFLOAT3 position)
{
	if (position.x >= _position.x &&
		position.x <= _position.x + _size.x &&
		position.z <= _position.z &&
		position.z >= _position.z - _size.z)
	{
		float x = ((position.x - _position.x) / _size.x) * (_maxWidth - 1);
		float z = ((_position.z - position.z) / _size.z) * (_maxDepth - 1);

		int x1 = floor(x);
		int x2 = x1 + 1;
		int z1 = floor(z);
		int z2 = z1 + 1;

		float h1 = _heightmap[z1 * _maxWidth + x1];
		float h2 = _heightmap[z1 * _maxWidth + x2];
		float h3 = _heightmap[z2 * _maxWidth + x1];
		float h4 = _heightmap[z2 * _maxWidth + x2];

		float xlerp1 = (x - x1) * h1 + (x2 - x) * h2;
		float xlerp2 = (x - x1) * h3 + (x2 - x) * h4;

		float zlerp = xlerp1 * (z - z1) + xlerp2 * (z2 - z);
		return zlerp;
	}
	return -1.0f;
}

void Terrain::LoadHeightMap(string filename)
{

}

void Terrain::Draw(ID3D11DeviceContext * pImmediateContext)
{
	pImmediateContext->IASetVertexBuffers(0, 1, &_geometry.vertexBuffer, &_geometry.vertexBufferStride, &_geometry.vertexBufferOffset);
	pImmediateContext->IASetIndexBuffer(_geometry.indexBuffer, DXGI_FORMAT_R16_UINT, 0);
	
	pImmediateContext->UpdateSubresource(_cb_resource, 0, nullptr, &_cb, 0, 0);

	_tb.minDistance = 100.0f;
	_tb.maxDistance = 500.0f;
	_tb.minTessPower = 1.0f;
	_tb.maxTessPower = 4.0f;
	_tb.minHeight = _position.y;
	_tb.maxHeight = _position.y + _size.y;
	
	if (_tessellation)
		pImmediateContext->HSSetConstantBuffers(1, 1, &_tessBuffer);
	else
		pImmediateContext->VSSetConstantBuffers(1, 1, &_tessBuffer);

	pImmediateContext->PSSetConstantBuffers(1, 1, &_tessBuffer);
	pImmediateContext->UpdateSubresource(_tessBuffer, 0, nullptr, &_tb, 0, 0);

	for (int i = 0; i < _textures.size(); ++i)
	{
		if (_tessellation)
			pImmediateContext->DSSetShaderResources(i + 2, 1, &_textures[i]);
		pImmediateContext->PSSetShaderResources(i + 2, 1, &_textures[i]);
	}

	if (_tessellation)
		pImmediateContext->DSSetShaderResources(1, 1, &_heightmapRV);
	else
		pImmediateContext->VSSetShaderResources(1, 1, &_heightmapRV);
	//pImmediateContext->PSSetShaderResources(1, 1, &_heightMap);

	/*if (_geometry.numberOfIndices > 65536)
	{
		UINT index = 0;
		while (index < _geometry.numberOfIndices + 65536)
		{
			pImmediateContext->DrawIndexed(65536, index, 0);
			index += 65536;
		}
	}*/

	pImmediateContext->DrawIndexed(_geometry.numberOfIndices, 0, 0);
}

void Terrain::AddTexture(const wchar_t* filename)
{
	ID3D11ShaderResourceView* rv;

	CreateDDSTextureFromFile(_device, filename, nullptr, &rv);
	_textures.push_back(rv);
}