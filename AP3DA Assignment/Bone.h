#ifndef BONE_H
#define BONE_H

#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include "DDSTextureLoader.h"
#include <vector>

using namespace DirectX;
using namespace std;

class Bone
{
private:
	XMFLOAT4X4 _toParent;
	Bone* _parent;
	vector<Bone*> _children;
	XMFLOAT3 _startPos;
	XMFLOAT3 _rotation;

	bool _cos = true;

public:
	Bone(){}
	Bone(XMFLOAT3 position, Bone* parent);
	~Bone();

	void AddChild(Bone* bone);
	void SetParent(Bone* parent){ _parent = parent; }
	Bone* GetParent(){ return _parent; }
	vector<Bone*> GetChildren(){ return _children; }

	void SetRotation(float x, float y, float z);
	void Update(UINT t);
	void SetCos(bool cos){ _cos = cos; }

	XMFLOAT4X4 GetToParentMatrix(){ return _toParent; }
	XMMATRIX GetWorldMatrix(XMMATRIX& world);
};

#endif