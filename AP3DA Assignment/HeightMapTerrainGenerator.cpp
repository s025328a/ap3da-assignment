#include "HeightMapTerrainGenerator.h"

HeightMapTerrainGenerator::HeightMapTerrainGenerator(string filename, int width, int height)
{
	_filename = filename;
	_width = width;
	_height = height;
}

Geometry HeightMapTerrainGenerator::GenerateTerrain(ID3D11Device* device, int rows, int columns, XMFLOAT3 size, XMFLOAT3 position, float maxTessPow)
{
	HRESULT hr;
	Geometry geometry;
	//vector<SimpleVertex> vertices;

	float width = size.x;
	float depth = size.z;
	float minHeight = position.y;
	float maxHeight = position.y + size.y;

	XMFLOAT3 scale = XMFLOAT3(width / (float)rows, maxHeight - minHeight, depth / (float)columns);
	float yOffset = minHeight;

	/*XMFLOAT3 half = XMFLOAT3((float)(rows + 1) * scale.x / 2.0f, 0.0f, (float)(columns + 1) * scale.z / 2.0f);*/
	int vertex_total = (rows + 1) * (columns + 1);
	int vertex_width = rows + 1;
	int vertex_height = columns + 1;

	XMFLOAT2 tex;

	// A height for each vertex 
	std::vector<unsigned char> in(_width * _height);

	
	float heightScale = scale.y;

	// Open the file.
	std::ifstream inFile;
	inFile.open(_filename.c_str(), std::ios_base::binary);

	if (inFile)
	{
		// Read the RAW bytes.
		inFile.read((char*)&in[0], (std::streamsize)in.size());
		// Done with file.
		inFile.close();
	}

	// Copy the array data into a float array and scale it. 
	mHeightmap.resize(_width * _height, 0);

	for (UINT i = 0; i < _width * _height; ++i)
	{
		mHeightmap[i] = ((in[i] / 255.0f) * heightScale) + yOffset;
	}

	float y = 0.0f;
	//float maxHeight = 0.25f;

	for (int i = 0; i < vertex_width; i++)
	{
		float x = i * scale.x - (width / 2.0f);
		tex.x = (float)i / rows;

		for (int j = 0; j < vertex_height; j++)
		{
			int a = ((i % _width) * _width);
			int b = ((j % _height));

			//y = mHeightmap[(i * vertex_height) + j];

			//y = mHeightmap[((i % _width) * _width) + (j % _height)];
			//y = mHeightmap[(i * _width) + j];
			y = minHeight;
			tex.y = (float)j / columns;
			vertices.push_back({ XMFLOAT3(x, y, -(j * scale.z - (depth / 2.0f))), XMFLOAT3(0.0f, 1.0f, 0.0f), tex });
		}
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * vertex_total;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &vertices[0];

	hr = device->CreateBuffer(&bd, &InitData, &geometry.vertexBuffer);

	geometry.vertexBufferOffset = 0;
	geometry.vertexBufferStride = sizeof(SimpleVertex);

	std::vector<WORD> indices;

	geometry.numberOfIndices = 0;

	for (int i = 0; i < rows; i++)
	{
		int start = i * vertex_height;
		for (int j = 0; j < columns; j++)
		{
			indices.push_back(start + j);
			indices.push_back(start + j + vertex_height);
			indices.push_back(start + j + 1);
			indices.push_back(start + j + vertex_height + 1);
			geometry.numberOfIndices += 4;
		}
	}

	ZeroMemory(&bd, sizeof(bd));

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * geometry.numberOfIndices;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;

	//D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = &indices[0];
	hr = device->CreateBuffer(&bd, &InitData, &geometry.indexBuffer);

	return geometry;
}

ID3D11ShaderResourceView* HeightMapTerrainGenerator::GetShaderResourceView(ID3D11Device* device)
{
	ID3D11ShaderResourceView* resource;
	HRESULT hr;

	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = _width;
	texDesc.Height = _height;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R32_FLOAT;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;

	// HALF is defined in xnamath.h, for storing 16-bit float.
	//std::vector<HALF> hmap(mHeightmap.size());
	//std::transform(mHeightmap.begin(), mHeightmap.end(),
	//	hmap.begin(), XMConvertFloatToHalf);

	D3D11_SUBRESOURCE_DATA data;
	data.pSysMem = &mHeightmap[0];
	data.SysMemPitch = _width*sizeof(float);
	data.SysMemSlicePitch = 0;
	ID3D11Texture2D* hmapTex = 0;
	
	hr = device->CreateTexture2D(&texDesc, &data, &hmapTex);

	if (FAILED(hr))
		return NULL;

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = -1;
	hr = device->CreateShaderResourceView(
		hmapTex, &srvDesc, &resource);

	if (FAILED(hr))
		return NULL;

	// SRV saves reference.
	hmapTex->Release();

	return resource;
}