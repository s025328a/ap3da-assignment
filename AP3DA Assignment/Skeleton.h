#ifndef SKELETON_H
#define SKELETON_H

#include "GameObject.h"
#include "Bone.h"

using namespace DirectX;

class Skeleton : public GameObject
{
private:
	Bone* _root;
	UINT time = 0;

public:
	Skeleton(){}
	Skeleton(string type, Geometry geometry, Material material);

	void Draw(ID3D11DeviceContext * pImmediateContext);
	void InitArm();
	void InitVertexData();

	void DrawBone(Bone* bone, ID3D11DeviceContext * pImmediateContext);
};

#endif